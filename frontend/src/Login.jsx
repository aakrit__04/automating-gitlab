import React , { useState } from "react";
import Nav from "./Nav.jsx";
import { useNavigate } from 'react-router-dom';
import { useEffect } from "react";


export const Login = (props) => {
    const navigate = useNavigate();

    useEffect(()=>{
        if(window.localStorage.getItem("log") === "admin")
        {
            navigate("/profile");
        }
    });
    const [user,setUser] = useState({
        username:'',pass:''
    });

    let name , value;
    const change = (e) =>
    {
        name = e.target.name;
        value = e.target.value;
        setUser({...user,[name]:value});
    }

    const handleSubmit = (e) => {
        if(user.username === "admin" && user.pass === "admin")
        {
            window.localStorage.setItem("log","admin")
            navigate("/profile");
        }
    }

    return (
        <div className="login justify-content-center">
            <Nav />
            <form>
                <input value={user.username} onChange={change} name="username" type="text" placeholder="Username"/>
                <input value={user.pass} onChange={change} name="pass" type="password" placeholder="Password"/>
                <button type="submit" onClick={handleSubmit}>Log in</button>
                <div className="my-class">
                    <p style={{color:"grey"}} className='link-btn' type="submit" onClick={() => props.onFormSwitch('register')}>Don't have an account? Register here</p>
                </div>
            </form>   
            
            <div className="signin">
                <h5>or sign in with :</h5>
                <i className="foot-icons fa-brands fa-twitter"></i>
                <i className="foot-icons fa-brands fa-apple"></i>
                <i className="foot-icons fa-brands fa-google"></i>
                <i className="foot-icons fa-brands fa-facebook"></i>
            </div>     
        </div>
    );
}
