import React , { useState } from "react";
import Nav2 from "./Nav2";
import { useNavigate } from 'react-router-dom';
import { useEffect } from "react";

export const Home = () => {
    return (
        <div>
            <div>
                <Nav2/>
            </div>
            <div>
                <h1>WELCOME TO HOME!!</h1>
            </div>
        </div>
    );
}