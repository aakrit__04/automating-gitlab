import React, { useState } from "react";
import Nav2 from "./Nav2";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { useEffect } from "react";

export const Profile = () => {
  const navigate = useNavigate();
  return(
    <div>
      <Nav2 />
      <div>
        <h1>Profile Page</h1>
      </div>
    </div>
  )
};
