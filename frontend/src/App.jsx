import React , {useState} from 'react';
import {Login} from "./Login.jsx";
import {Register} from "./Register.jsx";
import {BrowserRouter, Routes, Route} from "react-router-dom";
import { Profile } from './Profile.jsx';
import { Aproject } from './Aproject.jsx';
import { Dproject } from './Dproject.jsx';
import { Cproject } from './Cproject.jsx';
import { Home } from './Home.jsx';


function App() {

    const [currform,setcurrform] = useState('login');
    const toggleForm = (formName) => {
    setcurrform(formName);
    }

    return(
      <div>
        <BrowserRouter>
            <Routes>
              <Route
                exact path="/"
                element={currform === "login" ? <Login onFormSwitch={ toggleForm }/> : <Register onFormSwitch={ toggleForm } />}
              />
              <Route
                exact path="/profile"
                element={<Profile/>}
              />
              <Route
                exact path="/aproject"
                element={<Aproject/>}
              />
              <Route
                exact path="/dproject"
                element={<Dproject/>}
              />
              <Route
                exact path="/cproject"
                element={<Cproject/>}
              />
              <Route
                exact path="/home"
                element={<Home/>}
              />
            </Routes>
        </BrowserRouter>
      </div>
      );
}

export default App;
