import React from "react";
import { useNavigate } from 'react-router-dom';

function Nav2()
{
    const navigate = useNavigate();
    return (
        <div>
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">
                  <div class="navbar-brand">BADAL</div>
                  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>
                    <div class="collapse navbar-collapse" id="navbarContent">
                        <ul class="navbar-nav ms-auto">
                            <li class="nav-item" onClick={() => {navigate("/home")}}>
                                <i style={{color:'black'}} class="fa-solid fa-home"></i>
                                <h4 style={{color:'black'}} type="submit">home</h4>
                            </li>
                            <li class="nav-item" type="submit" onClick={() => {navigate("/profile")}}>
                                <h4 style={{color:'black'}}>Profile</h4>
                            </li>
                            <li class="nav-item" type="submit" onClick={() => {navigate("/aproject")}}>
                                <h4 style={{color:'black'}}>Alloted Projects</h4>
                            </li>
                            {/* <li class="nav-item" type="submit" onClick={() => {navigate("/dproject")}}>
                                <p style={{color:'black'}}>Project Description</p>
                            </li> */}
                            {/* <li class="nav-item" type="submit" onClick={() => {navigate("/cproject")}}>
                                <p style={{color:'black'}}>Project Code</p>
                            </li> */}
                            <li class="nav-item" onClick={() => {window.localStorage.removeItem("localstr");window.localStorage.removeItem("log");navigate("/")}} type="submit">
                                <h4>Logout</h4>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    );
}

export default Nav2;