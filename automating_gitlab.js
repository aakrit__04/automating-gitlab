const express = require('express');
const cors = require('cors');
const fetch = require('node-fetch');
const https = require('https');
const app = express();
app.use(express.json());
app.use(cors());


const accessToken = 'glpat-5ytbP98xj9Phhq3YZRKs';
const projectId = '43976975';


// creating an issue
app.post("/create_issue" , (req,res)=>{
  const issueTitle = req.body.issueTitle;
  const issueDescription = req.body.issueDescription;
  const issueLabels = req.body.issueLabels;
  fetch(`https://gitlab.com/api/v4/projects/${projectId}/issues`, {
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${accessToken}`,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      title: issueTitle,
      description: issueDescription,
      labels: issueLabels
    })
  })
  .then((response) => {
    if (!response.ok) {
      throw new Error(`HTTP error: ${response.status}`);
    }
    return response.json();
  })
  .then((data) => {
    // console.log('Issue created:', data);
    res.send({message : data});
  })
  .catch((error) => {
    console.error('Error creating issue:', error);
  });
});




// deleting an issue
app.delete("/delete_issue", (req,res)=>{
  const issueId = req.body.issueId;
  fetch(`https://gitlab.com/api/v4/projects/${projectId}/issues/${issueId}`, {
    method: 'DELETE',
    headers: {
      'Authorization': `Bearer ${accessToken}`,
    }
  })
  .then((response) => {
    console.log("Deleted issue");
    res.send({message : "DELETED ISSUE"})
  })
});




// removing a file from the master branch 
app.delete("/removefile_master", (req,res)=>{
  const branchName1 = 'master';
  const filePath = req.body.path;
  fetch(`https://gitlab.com/api/v4/projects/${projectId}/repository/files/${encodeURIComponent(filePath)}?branch=${encodeURIComponent(branchName1)}&commit_message=File+removed`, {
    method: 'DELETE',
    headers: {
      'Authorization': `Bearer ${accessToken}`,
      'Content-Type': 'application/json'
    }
  })
  .then((response) => {
    console.log("Deleted file from master branch");
    res.send({message : "DELETED FILE"})
  })
});



// deleting a branch from gitlab
app.delete("/delete_branch", (req,res)=>{
  const branchName = req.body.branchName;
  fetch(`https://gitlab.com/api/v4/projects/${projectId}/repository/branches/${encodeURIComponent(branchName)}`, {
    method: 'DELETE',
    headers: {
      'Authorization': `Bearer ${accessToken}`,
    }
  })
  .then((response) => {
    console.log("Deleted branch");
    res.send({message : "DELETED BRANCH"})
  })

})


app.listen(5050, ()=>{
    console.log("Server started on 5050")
})
